﻿using UnityEngine;
using System.Collections;

public class ThirdPersonCamera : MonoBehaviour {

    public GameObject cameraTarget;

    public float rotateSpeed = 1;
    float rotateDir; 

    //Camera distance from target
    public float offsetDistance = 8;
    public float offsetHeight = 8;

    public float smoothing = 5;

    Vector3 offset;
    Vector3 lastPosition;

	void Start () {
        cameraTarget = GameObject.Find("Knight");

        offset = new Vector3(cameraTarget.transform.position.x, cameraTarget.transform.position.y + offsetHeight, cameraTarget.transform.position.z - offsetDistance);
        lastPosition = offset;
    }
	
	void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            rotateDir = -1;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            rotateDir = 1;
        }
        else rotateDir = 0;


        //update position of camera
        offset = Quaternion.AngleAxis(rotateDir * rotateSpeed, Vector3.up) * offset;
        transform.position = cameraTarget.transform.position + offset;
        //Smooth the transition by lerping btw the last pos and new pos
        transform.position = new Vector3(Mathf.Lerp(lastPosition.x, cameraTarget.transform.position.x + offset.x, smoothing * Time.deltaTime),
                             Mathf.Lerp(lastPosition.y, cameraTarget.transform.position.y + offset.y, smoothing * Time.deltaTime),
                             Mathf.Lerp(lastPosition.z, cameraTarget.transform.position.z + offset.z, smoothing * Time.deltaTime));

        //lool at target
        transform.LookAt(cameraTarget.transform.position);
    }

    void LateUpdate()
    {
        lastPosition = transform.position;
    }
}
