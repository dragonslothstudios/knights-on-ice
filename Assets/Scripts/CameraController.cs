﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    bool camIsMoving;
    bool gameOver = false;
    bool lerpOver = false;
    float camSpeed = 0.1f;
    float error = 0.001f;
    Vector3 targetLocation;
    Camera cam;

	// Use this for initialization
	void Start () {
        //transform.position = new Vector3(0, 38, -7);
        cam = GetComponent<Camera>();

    }

    // Update is called once per frame
    void Update () {
        if (camIsMoving)
        {
            transform.position = Vector3.Lerp(transform.position, targetLocation, camSpeed);

            if (transform.position == targetLocation)
            { camIsMoving = false; print("Cam has been updated"); }
        }

        if(gameOver)
        {
            transform.position = Vector3.Lerp(transform.position, targetLocation, camSpeed);

            if(transform.position.y >= targetLocation.y - error && transform.position.y <= targetLocation.y + error)
            {
                print("Lerpover");
                lerpOver = true;
                gameOver = false;
                camSpeed = camSpeed / 1.5f;
                cam.nearClipPlane = 0.3f;
                targetLocation = new Vector3(transform.position.x, -10, -5);
            }
        }
        if(lerpOver)
        {            
            transform.position = Vector3.Lerp(transform.position, targetLocation, camSpeed);
            float f = Mathf.Lerp(cam.fieldOfView, 60.0f, camSpeed);
            cam.fieldOfView = f;

            if (transform.position == targetLocation)
            {
                lerpOver = false;
            }
        }
    }

    public bool MoveCam()
    {
        //Vector3 move = new Vector3(0, -1, 0);
        //transform.Translate(move);
        targetLocation = new Vector3(transform.position.x, transform.position.y - 15, transform.position.z);
        camIsMoving = true;
        
         
        return true;

    }
    public void GameOverMoveCam()
    {
        gameOver = true;
        lerpOver = false;
        targetLocation = new Vector3(transform.position.x, 32, transform.position.z);
    }
}
