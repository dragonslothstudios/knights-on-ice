﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shieldController : MonoBehaviour {

    bool keyPressed = false;
    private Vector3 offset;
    private float sOffset;
    public string button;
    private PlayerController p;
    Animator anim;
    float downTime = 0.0f;

    public GameObject OnHitParticle;

    //throwing mechanic
    bool attack = false; 
    bool smash = false;
    bool thrown = false;

    //Start and end positions
    Vector3 startPos;
    Vector3 endPos;

    //Calculate distance the shield will go
    float throwDistance;
    float throwHeight = 5.0f;
    float incrementor = 0.4f; 

    // Use this for initialization
    void Start () {

        p = GetComponentInParent<PlayerController>();
        anim = p.GetComponent<Animator>();
        
        if(p.playerNumber == 1)
        {
            button = "Fire1";
        }
        else if (p.playerNumber == 2)
        {
            button = "Fire2";
        }

       // transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        //this.transform.position = new Vector3(this.transform.position.x, 2.2f, this.transform.position.z + 10.0f);
        offset = new Vector3(0.0f, -1.2f, 1.0f);
        sOffset = 1.0f;

    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetButtonDown(button) && p.getGrounded())
        {
            p.canMove = false;
            anim.SetBool("isBashing", true);
            //timer for smash vs throw
            downTime = Time.time;
           
 
            throwDistance += 1f;
            print(throwDistance);           

            if (!keyPressed)
            {
                keyPressed = true;
               // transform.localPosition = Vector3.zero;
               
            }
        }
        if (Input.GetButtonUp(button))
        {
            downTime = Time.time - downTime;
            attack = true;
            print(downTime);

            //anim.StopPlayback();


            if (downTime < 0.5f)
            {
                startPos = transform.position;
                endPos = transform.position + (transform.forward * 1.1f) + new Vector3(0, -2, 0);
                smash = true;
            }
            else
            {
                startPos = transform.position;
                endPos = transform.position + (transform.forward * 5) + new Vector3(0, -2, 0);
                thrown = true;
            }
            throwDistance = 0.0f;
        }

        float animTime = Time.time - downTime;
        if(keyPressed && (animTime > 0.5f))
        {
            anim.enabled = false;
        }


        if(attack)
        {

            anim.enabled = true;
            anim.SetBool("isBashing", false);

            if (thrown)
            {
                incrementor += 0.04f;
                Vector3 currentPos = Vector3.Lerp(startPos, endPos, incrementor);
                currentPos.y += throwHeight * Mathf.Sin(Mathf.Clamp01(incrementor) * Mathf.PI);
                transform.position = currentPos;
            }
            if (smash)
            {
                incrementor += 0.4f;
                Vector3 currentPos = Vector3.Lerp(startPos, endPos, incrementor);
                transform.position = currentPos;
            }

            if (transform.position == endPos)
            {
                attack = false;
                thrown = false;
                smash = false;
                incrementor = 0;
                Vector3 tempPos = startPos;
                startPos = transform.position;
                endPos = tempPos;

                p.canMove = true;
                keyPressed = false;
                transform.localRotation = Quaternion.identity;
                this.transform.position = new Vector3(this.transform.position.x, 1.2f, this.transform.position.z);
            }
        }


        if (!keyPressed)
        {
            if (!thrown)
            {
                //print(this.transform.localPosition);
                //transform.localPosition = Vector3.zero;
                this.transform.localRotation = Quaternion.identity;
                if (p.transform.localRotation.y > 0.05f)
                {
                    Vector3 rotNorm = new Vector3(0.0f, 0.0f, Mathf.Abs(p.transform.localRotation.y));
                    rotNorm.Normalize();
                    rotNorm.y = 1.2f / sOffset;
                    this.transform.localPosition = rotNorm * sOffset;
                }
                else
                {
                    this.transform.localPosition = new Vector3(0.0f, 1.2f / sOffset, sOffset);
                }
                //
                //this.transform.position = new Vector3(this.transform.position.x, 2.2f, this.transform.position.z);
            }
        }

    }

    void OnTriggerEnter(Collider target)
    {
        //print(target.gameObject);
        Instantiate(OnHitParticle, transform.position, Quaternion.identity);

        if (target.gameObject.name == "Floor_Tile" || target.gameObject.name == "Floor_Tile2" || target.gameObject.name == "Floor_Tile3")
        {
            //print("Destroy");

            target.gameObject.SetActive(false);
        }

        if (target.gameObject.name == "Player(Clone)")
        {
            Rigidbody rb = p.GetComponent<Rigidbody>();
            PlayerController colPc = target.GetComponent<PlayerController>();
            colPc.beingHit = true;
            colPc.knockPos = new Vector3(colPc.transform.position.x + rb.velocity.x, colPc.transform.position.y + 3, colPc.transform.position.z + rb.velocity.z);


            //Rigidbody colRb = target.GetComponent<Rigidbody>();
            //Rigidbody rb = p.GetComponent<Rigidbody>();
            //
            //if(colRb != null && rb != null)
            //{
            //    //colRb.AddExplosionForce(1000, new Vector3(transform.position.x, transform.position.y, transform.position.z), 100);
            //    print("Collision with player from shield: " + rb.velocity.x + "    " + rb.velocity.z);
            //
            //    //colRb.AddForce(rb.velocity.x * 1000, 10, rb.velocity.z * 1000, ForceMode.Acceleration);
            //    colRb.AddForce(rb.velocity.x * 1000, 10, rb.velocity.z * 1000);
            //    //colRb.AddExplosionForce(500, new Vector3(rb.position.x, 1, rb.position.z), 100, 3);
            //
            //}
            //target.transform.parent

            //colRb.AddForce(transform.forward * 200, ForceMode.Impulse);
        }

        
    }
}
