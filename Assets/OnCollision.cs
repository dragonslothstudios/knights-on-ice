﻿using UnityEngine;
using System.Collections;

public class OnCollision : MonoBehaviour {


    public GameObject OnHitParticle;
    public Camera cam;
    Vector3 dir;
    Vector3 camPosition;

    void Start () {

	}
	
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Floor")
        {
            camPosition = cam.transform.position;
            dir = (transform.position - camPosition).normalized;
            Instantiate(OnHitParticle, transform.position - (dir*0.2f), Quaternion.identity);
            //Instantiate(OnHitParticle, transform.position, Quaternion.identity);
        }
    }

}
