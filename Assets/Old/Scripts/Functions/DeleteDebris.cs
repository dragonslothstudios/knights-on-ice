﻿using UnityEngine;
using System.Collections;

public class DeleteDebris : MonoBehaviour {

    public float deleteTime = 5.0f;
    private float originalTime;


    // Use this for initialization
    void Start () {
        originalTime = deleteTime;
    }
	
	// Update is called once per frame
	void Update () {
            deleteTime -= Time.deltaTime;

            if (deleteTime <= 0)
            {
                Destroy(gameObject);
                deleteTime = originalTime;
            }
    }
}
