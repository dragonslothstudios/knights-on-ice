﻿using UnityEngine;
using System.Collections;

public class Highlight : MonoBehaviour
{
    private Color startColour;

    public Renderer rend;
    private Color original;

    void Start()
    {
        rend = GetComponent<Renderer>();
        original = rend.material.color; //Keep a copy of the original color
    }

    void OnMouseEnter()
    {
        rend.material.color = Color.blue; //Change to this color when hovering over
    }

    void OnMouseExit()
    {
        rend.material.color = original;
    }
}
