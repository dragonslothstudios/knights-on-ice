﻿using UnityEngine;
using System.Collections;

public class DestroyOnOutOfBoundary : MonoBehaviour {

    public float distance;
	
	// Update is called once per frame
	void Update () {
	
        if(transform.position.y <= distance)
        {
            //print("oob");
            Destroy(gameObject);
        }
	}
}
