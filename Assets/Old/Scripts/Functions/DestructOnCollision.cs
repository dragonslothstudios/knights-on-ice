﻿using UnityEngine;
using System.Collections.Generic;

public class DestructOnCollision : MonoBehaviour
{
    //public List<GameObject> deactivatedTiles = new List<GameObject>();
    public ParticleSystem ParticleTile;

    void OnCollisionEnter(Collision col)
    {
        //Destroy(col.gameObject);

        //deactivatedTiles.Add(col.gameObject);
        print(col.gameObject);

        if (col.gameObject.name == "Floor_Tile")
        {
            Vector3 particleSpawn = new Vector3(col.transform.position.x, 1, col.transform.position.z);
            Instantiate(ParticleTile, particleSpawn, Quaternion.identity);
            col.gameObject.SetActive(false);
        }
    }
}
