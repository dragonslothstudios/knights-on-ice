﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;              
    private LevelGenerator levelManager;

    //public PlayerController player1;
    //public PlayerController player2;

    public PlayerController player1;
    public PlayerController player2;

    int numDeath1;
    int numDeath2;

    public Material yellow;
    public Material green;

    bool gameOver = false;

    private Vector3 spawnP1;
    private Vector3 spawnP2;

    public static int GAME_STATE = 0;
    //0 = animation (opening, switch between levels)
    //1 = level 1
    //2 = level 2
    //3 = over
    public static bool MOVE_CAMERA = false;
    Camera gameCam;
    CameraController gameCamCont;

    // Use this for initialization
    void Start()
    {
    }


    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        
        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);

        //Call the InitGame function to initialize the first level 
        InitGame();
    }

    void InitGame()
    {
        GAME_STATE = 0;

        InitPlayer1();
        InitPlayer2();

        //Instantiate(gameCam, camSpawn, Quaternion.Euler(45, 0, 0));
        gameCam = GameObject.Find("GameCamera").GetComponent<Camera>();

        //gameCam.enabled = true;
        gameCamCont = gameCam.GetComponent<CameraController>();

        player1 = GameObject.FindGameObjectWithTag("player1").GetComponent<PlayerController>();
        player2 = GameObject.FindGameObjectWithTag("player2").GetComponent<PlayerController>();

        player1.GetComponentInChildren<SkinnedMeshRenderer>().material = yellow;
        player2.GetComponentInChildren<SkinnedMeshRenderer>().material = green;

        player1.transform.position = new Vector3(-8, 31, -8);
        player2.transform.position = new Vector3(8, 31, 8);

        GAME_STATE = 1;
    }

    void InitPlayer1()
    {
        spawnP1 = new Vector3(-8, 31, -8);
        player1.playerNumber = 1;
        player1.tag = "player1";
        Instantiate(player1, spawnP1, Quaternion.identity);

    }
    
    void InitPlayer2()
    {
        spawnP2 = new Vector3(8, 31, 8);
        player2.playerNumber = 2;
        player2.tag = "player2";
        Instantiate(player2, spawnP2, Quaternion.identity);
    }

    void updateLevel()
    {

        
        print(GAME_STATE);
        //Update level of game
        if (GAME_STATE < 3)
        {
            gameCam = GameObject.Find("GameCamera").GetComponent<Camera>();
            gameCam.GetComponent<CameraController>().MoveCam();
            GAME_STATE += 1;
        }
        else
        {
        }

        //Reset players to prep for next level
        player1.setDead(false);
        player2.setDead(false);

        switch (GAME_STATE)
        {
            case 1:
                player1.setPos(new Vector3(-8, 31, -8));
                player2.setPos(new Vector3(8, 31, 8));
                break;
            case 2:
                player1.setPos(new Vector3(-8, 16, -8));
                player2.setPos(new Vector3(8, 16, 8));
                break;
            case 3:
                player1.setPos(new Vector3(-8, 1, -8));
                player2.setPos(new Vector3(8, 1, 8));
                break;
        }
    }

    void GameOver(PlayerController winner)
    {
        gameOver = true;
        GAME_STATE = 4;
        print("Player 2 Wins");
        winner.setPos(new Vector3(0, -15, 0));
        winner.transform.rotation = new Quaternion(0, 180, 0, 0);
        gameCamCont.GameOverMoveCam();

    }

  

    // Update is called once per frame
    void FixedUpdate () {
        //gameCam = GameObject.Find("GameCamera(clone)").GetComponent<Camera>();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyUp(KeyCode.Space) && GAME_STATE == 4)
        {
            SceneManager.LoadScene("Menu");
            numDeath1 = 0;
            numDeath2 = 0;
            gameOver = false;
        }


        if(player1 != null)
            player1 = GameObject.FindGameObjectWithTag("player1").GetComponent<PlayerController>();
        if(player2 != null)
            player2 = GameObject.FindGameObjectWithTag("player2").GetComponent<PlayerController>();

        //print(player1.gameObject.transform.position);

        


        if (player1 != null)
        {
            if (player1.getDead())
            {
                if (numDeath1 < 1)
                {
                    numDeath1++;
                    updateLevel();
                    print("player1 died");
                }
                else
                {
                    if(!gameOver)
                    GameOver(player2);
                }
            }
        }
        if (player2 != null)
        {
            if (player2.getDead())
            {
                if (numDeath2 < 1)
                {
                    numDeath2++;
                    updateLevel();
                    print("player2 died");
                }
                else
                {
                    if(!gameOver)
                    GameOver(player1);
                }
            }
        }


    }

    public void updatePlayers(PlayerController p)
    {
        //gameCam.transform.position += new Vector3(0, -1, 0);

        //Update();   

    }

    public bool getCamState()
    {
        return MOVE_CAMERA;
    }

    public int getGameState()
    {
        return GAME_STATE;
    }
}
