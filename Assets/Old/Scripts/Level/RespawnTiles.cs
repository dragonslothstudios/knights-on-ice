﻿using UnityEngine;
using System.Collections;

public class RespawnTiles : MonoBehaviour {

    Transform[] allChildren;
    public float tileRespawnTime = 5.0f;
    private float originalTime;

    private bool hasSetChildren;

    public void SetChildren()
    {
        allChildren = GetComponentsInChildren<Transform>();
    }

    void Start()
    {
        originalTime = tileRespawnTime;
    }

    void FixedUpdate()
    {
        //if(Input.GetKeyUp("x"))
        //{
        //    SetChildren();
        //}
        //if(Input.GetKeyUp("z"))
        //{
        //    foreach (Transform child in allChildren)
        //    {
        //        print(child.gameObject.activeSelf);
        //    }
        //}
       // if(Input.GetKeyUp("c"))
       // {
       //     foreach (Transform child in allChildren)
       //     {
       //         if (!child.gameObject.activeSelf)
       //         {
       //             print("ITS BACK");
       //             child.gameObject.SetActive(true);
       //         }
       //     }
       // }
       // 
    }


    // Update is called once per frame
    void Update () {

        //SetChildren();
        if(hasSetChildren)
        {
            foreach (Transform child in allChildren)
            {
                if (!child.gameObject.activeSelf)
                {
                    tileRespawnTime -= Time.deltaTime;
            
                    if(tileRespawnTime <= 0)
                    {
                        child.gameObject.SetActive(true);
                        tileRespawnTime = originalTime;
                    }
                }
            }
        }
        else
        {
            SetChildren();
            hasSetChildren = true;
        }
           

    }
}

