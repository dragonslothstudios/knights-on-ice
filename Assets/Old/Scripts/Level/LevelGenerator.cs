﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

    public Transform floorTile;
    public Transform floorTile2;
    public Transform floorTile3;
    public Vector3 levelSize;
    private int a;
    private int b;
    private int c;
    Renderer r;

    //public RespawnTiles respawner;

    [Range(0,1)]
    public float tileSize;

    void Start()
    {
        GenerateLevel();
        //respawner.SetChildren();
    }

    public void GenerateLevel()
    {
        //Creates an object to use as a folder
        string folderName = "Level 1";
        string folderName2 = "Level 2";
        string folderName3 = "Level 3";
        if (transform.FindChild(folderName))
        {
            DestroyImmediate(transform.FindChild(folderName).gameObject);
        }

        Transform levelHolder = new GameObject (folderName).transform;
        Transform levelHolder2 = new GameObject(folderName2).transform;
        Transform levelHolder3 = new GameObject(folderName3).transform;
        levelHolder.parent = transform;

        //Creates tile based level
        for (int i = 0; i < 3; i++)
        {
            for (int x = 0; x < levelSize.x; x++)
            {
                for (int y = 0; y < levelSize.y; y++)
                {
                    for (int z = 0; z < levelSize.z; z++)
                    {
                        if (i == 0)
                        {
                            a++;
                            Vector3 tilePosition = new Vector3(-levelSize.x / 2 + 0.5f + x, -levelSize.y / 2 + 0.5f + y + (i * 15), -levelSize.z / 2 + 0.5f + z);
                            Transform newTile = Instantiate(floorTile, tilePosition, Quaternion.Euler(Vector3.zero)) as Transform;
                            newTile.name = floorTile.name;
                            //newTile.localScale = Vector3.one * (1 - tileSize); //edits size of tile
                            newTile.parent = levelHolder; //Put each tile under object folder
                            Transform child;
                            child = newTile.FindChild("Top");
                            //Texture2D tex = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/images/Lava/Lava_Floor_" + a + ".jpg", typeof(Texture2D));
                            Texture2D tex = (Texture2D)Resources.Load("Lava/Lava_Floor_" + a);
                            child.GetComponent<Renderer>().material.SetTexture("_MainTex", tex);
                            //levelHolder.gameObject.SetActive(false);
                        }
                        if (i == 1)
                        {
                            b++;
                            Vector3 tilePosition = new Vector3(-levelSize.x / 2 + 0.5f + x, -levelSize.y / 2 + 0.5f + y + (i * 15), -levelSize.z / 2 + 0.5f + z);
                            Transform newTile = Instantiate(floorTile2, tilePosition, Quaternion.Euler(Vector3.zero)) as Transform;
                            newTile.name = floorTile2.name;
                            //newTile.localScale = Vector3.one * (1 - tileSize); //edits size of tile
                            newTile.parent = levelHolder2; //Put each tile under object folder
                            Transform child;
                            child = newTile.FindChild("Top");
                            //Texture2D tex = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/images/Stone/Stone_Floor_" + b + ".jpg", typeof(Texture2D));
                            Texture2D tex = (Texture2D)Resources.Load("Stone/Stone_Floor_" + b);
                            child.GetComponent<Renderer>().material.SetTexture("_MainTex", tex);
                            //levelHolder2.gameObject.SetActive(false);
                        }
                        if (i == 2)
                        {
                            c++;
                            Vector3 tilePosition = new Vector3(-levelSize.x / 2 + 0.5f + x, -levelSize.y / 2 + 0.5f + y + (i * 15), -levelSize.z / 2 + 0.5f + z);
                            Transform newTile = Instantiate(floorTile3, tilePosition, Quaternion.Euler(Vector3.zero)) as Transform;
                            newTile.name = floorTile3.name;
                            //newTile.localScale = Vector3.one * (1 - tileSize); //edits size of tile
                            newTile.parent = levelHolder3; //Put each tile under object folder
                            Transform child;
                            child = newTile.FindChild("Top");
                            //Texture2D tex = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/images/Ice/IceTile_" + c + ".jpg", typeof(Texture2D));
                            Texture2D tex = (Texture2D)Resources.Load("Ice/IceTile_" + c);
                            child.GetComponent<Renderer>().material.SetTexture("_MainTex", tex);
                        }
                    }
                }
            }
        }
    }
   
}
