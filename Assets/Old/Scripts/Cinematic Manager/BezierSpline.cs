﻿using UnityEngine;
using System;
using System.Collections;

public enum Modes
{
    Isolated,
    Aligned,
    Mirrored
}

public class BezierSpline : MonoBehaviour {

    [SerializeField]
    private Vector3[] points; //control points

    [SerializeField]
    private Modes[] modes; //modes between the curves segements


    //------------------------------------------------Managing Modes

    //return mode at control point, modes are stored between points - 3rd point of the first, and second of the next segment  ( first 2 points = mode[0], following groups of 3 points = mode[increment per group]
    //  p0 --- p1 --- p2 ---p3 | p4 --- p5 ---p6 | p7 --- p8 ---p9     Control points index
    //  0 ---- 0 ---- 1 ----1  | 1 ---- 2 ---- 2 | 2 ---- 3 ----3     Modes index
    public Modes GetControlPointMode(int i)
    {
        return modes[(i + 1) / 3];
    }

    public void SetControlPointMode(int i, Modes mode)
    {
        modes[(i + 1) / 3] = mode;
        Adjust(i);
    }

    //Have the control points follow constraints (prevent sharp curves)
    private void Adjust(int i)
    {
        int modeIndex = (i + 1) / 3;
        Modes mode = modes[modeIndex];  //mode of the selected index

        //if it is set to free, or at curve endpoints, do nothing
        if(mode == Modes.Isolated || modeIndex == 0 || modeIndex == modes.Length - 1) { return; }

        //control point index in the middle of segment
        int middleIndex = modeIndex * 3;
        
        //adjust point on opposite side of middle index while keeping the other one fixed
        int fixedIndex, adjustedIndex;
        //end point on current segment is adjusted if middle/initial point is selected (if middle point is selected, both end points on segment is moved along with it ---- under SetControlPoint();
        if (i <= middleIndex)
        {
            fixedIndex = middleIndex - 1;
            adjustedIndex = middleIndex + 1;
        }
        else //opposite
        {
            fixedIndex = middleIndex + 1;
            adjustedIndex = middleIndex - 1;
        }

        //Mirror line around middle point
        Vector3 middlePoint = points[middleIndex]; //get middle point
        Vector3 adjustedVector = middlePoint - points[fixedIndex]; //get the vector of middlepoint - fixedpoint

        //if aligned, new vector has the same distance as its previous state
        if (mode == Modes.Aligned)
        {
            adjustedVector = adjustedVector.normalized * Vector3.Distance(middlePoint, points[adjustedIndex]);
        }

        points[adjustedIndex] = middlePoint + adjustedVector; //Invert it by adding it to middle point

       
    }
    
    //--------------------------------------------------------------


    //------------------------------------------------Managing Control Points 
    public int numberOfControlPoints()
    {
        return points.Length;
    }

    public Vector3 GetControlPoint(int index)
    {
        return points[index];
    }

    public void SetControlPoint(int i, Vector3 point)
    {
       //if the point selected is a middle point
       if (i % 3 == 0)
       {
           Vector3 displacement = point - points[i]; //vector from old -> new position of control point

           if (i > 0)
           {
               //if there is a previous control point, move it by the same amount as the middle point
               points[i - 1] += displacement;
           }
           if (i < points.Length - 1)
           {
               //if there is a next control point, move it by the same amount as the middle point
               points[i + 1] += displacement;
           }
            
       }
        points[i] = point;
        Adjust(i);
    }
    //-------------------------------------------------------------
   
    //get point on curve at interpolation value t
    public Vector3 GetPoint(float t)
    {
        //index of the first point of every segment
        int i;
        //if t = 1, final point on curve
        if (t >= 1)
        {
            t = 1;
            i = points.Length - 4;
        }
        //otherwise, get interpolation value t of curve 
        else
        {
            t = Mathf.Clamp01(t) * CurveCount();
            i = (int)t;
            t -= i;
            i *= 3;
        }

       //Debug.Log("t: " + t + "i: " + i);
        return transform.TransformPoint(Bezier.GetPoint(points[i], points[i+1], points[i+2], points[i+3], t));
        //return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], points[3], t));
    }

    //get velocity on curve at interpolation value t
    public Vector3 GetVelocity(float t)
    {
        int i;
        if (t >= 1f)
        {
            t = 1f;
            i = points.Length - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount();
            i = (int)t;
            t -= i;
            i *= 3;
        }

        return transform.TransformPoint(Bezier.GetFirstDerivative(points[i], points[i + 1], points[i + 2], points[i + 3], t)) - transform.position;
        //return transform.TransformPoint(Bezier.GetFirstDerivative(points[0], points[1], points[2], points[3], t)) - transform.position;
    }

    //normalized velocity
    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }

    //Increase array size enough to fit a new curve (3 points), set the x of each new point to be 1 more of the previous x in the x axis
    public void AddCurve()
    {
        Vector3 point = points[points.Length - 1];
        Array.Resize(ref points, points.Length + 3);
        point.x += 1f;
        points[points.Length - 3] = point;
        point.x += 1f;
        points[points.Length - 2] = point;
        point.x += 1f;
        points[points.Length - 1] = point;

        //Increase array size of modes by 1, newest index is the same as the previous
        Array.Resize(ref modes, modes.Length + 1);
        modes[modes.Length - 1] = modes[modes.Length - 2];

        Adjust(points.Length - 4);
    }

    //return number of curves (segments of 3points + 1)
    public int CurveCount()
    {
        return (points.Length - 1) / 3;  
    }

    //Reset to base values
    public void Reset()
    {
        points = new Vector3[] {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f),
            new Vector3(4f, 0f, 0f)
        };

        modes = new Modes[] {
            Modes.Isolated,
            Modes.Isolated
        };
    }
}
