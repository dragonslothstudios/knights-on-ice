﻿using UnityEngine;
using System.Collections;

public static class Bezier{

    //Get Point along curve
	public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);

        //B(t) = (1 - t)^2 P0 + 2(1 - t) t P1 +t^2 P2 -- Quadratic

        //return ((1 - t) * (1 - t) * p0) +  
        //        (2 * (1 - t) * t * p1) + 
        //        (t * t * p2) ;

        //B(t) = (1 - t)^3 P0 + 3(1 - t)^2 t P1 +3(1 - t) t^2 P2 +t^3 P3 -- Cubic 
        return (1 - t) * (1 - t) * (1 - t) * p0 +
               3 * (1 - t) * (1 - t) * t * p1 +
               3 * (1 - t) * t * t * p2 +
               t * t * t * p3;
    }

    //First derivative (velocity)
    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);

        //B'(t) = 2 (1 - t) (P1 - P0) + 2 t (P2 - P1)  -- Quadratic

        //return 2 * (1 - t) * (p1 - p0) + 
        //       2 * t * (p2 - p1);


        //B'(t) = 3 (1 - t)^2 (P1 - P0) + 6 (1 - t) t (P2 - P1) + 3 t^2 (P3 - P2) -- Cubic

        return 3 * (1 - t) * (1 - t) * (p1 - p0) + 
               6 * (1 - t) * t * (p2 - p1) +
               3 * t * t * (p3 - p2);
    }

   
}
