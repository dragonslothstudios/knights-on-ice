﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggles : MonoBehaviour {

    Transform light1;
    Transform light2;
    Transform light3;
    Light l1;
    Light l2;
    Light l3;

    // Use this for initialization
    void Start () {

        light1 = transform.Find("LightIce");
        light2 = transform.Find("LightStone");
        light3 = transform.Find("LightLava");

        l1 = light1.GetComponent<Light>();
        l2 = light2.GetComponent<Light>();
        l3 = light3.GetComponent<Light>();

    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown("1"))
        {
            l1.enabled = !l1.enabled;
            l2.enabled = !l2.enabled;
            l3.enabled = !l3.enabled;

        }
		
	}
}
