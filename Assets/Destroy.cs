﻿using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour
{
    public float selfDestructTimer = 2;

    void Start()
    {
        Destroy(gameObject, selfDestructTimer);
    }
}