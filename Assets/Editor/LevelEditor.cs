﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (LevelGenerator))] //Defines this script as an editor for LevelGenerator
public class LevelEditor : Editor {

    //Custom inspector
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI(); //Draws default inspector stuff

        LevelGenerator level = target as LevelGenerator; //target is the object that this map editor is an editor of, cast it to a levelgenerator script


        if (GUILayout.Button("Generate Grid"))
        {
            level.GenerateLevel(); //allows updating in real time
        }

    }
}
